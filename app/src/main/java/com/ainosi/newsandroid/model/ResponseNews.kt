package com.ainosi.newsandroid.model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName

class ResponseNews(
    @field:SerializedName("status")
    val status: String? = null,
    @field:SerializedName("totalResults")
    val totalResults: Int,
    @field:SerializedName("articles")
    val results: List<NewsModel>
){
    class Deserializer : ResponseDeserializable<ResponseNews> {
        override fun deserialize(content: String) = Gson().fromJson(content, ResponseNews::class.java)
    }
}
