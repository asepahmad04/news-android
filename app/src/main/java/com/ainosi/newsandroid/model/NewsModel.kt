package com.ainosi.newsandroid.model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


data class NewsModel (
    var author: String,
    var source: Source,
    var title: String,
    var description: String ,
    var url: String?,
    var urlToImage: String,
    var publishedAt: String,
    var content: String
) {

    data class Source(
        var id: String,
        var name: String
    ){
        class Deserializer : ResponseDeserializable<Source> {
            override fun deserialize(content: String) = Gson().fromJson(content, Source::class.java)
        }
    }

    class ListDeserializer : ResponseDeserializable<List<NewsModel>> {
        override fun deserialize(content: String) =
            Gson().fromJson<List<NewsModel>>(content, object : TypeToken<List<NewsModel>>() {}.type)
    }
}
