package com.ainosi.newsandroid

import android.app.Application
import com.ainosi.newsandroid.common.di.appModule
import com.ainosi.newsandroid.common.utils.Logger
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class App : Application(){

    override fun onCreate() {
        super.onCreate()
        Logger.init()
        startKoin{
            androidLogger()
            androidContext(this@App)
            modules(appModule)
        }
    }
}