package com.ainosi.newsandroid.common.utils

import com.google.gson.Gson

import org.json.JSONArray
import org.json.JSONException

import java.util.ArrayList

object Gxon {
    fun to(o: Any): String {
        val gson = Gson()
        return gson.toJson(o)
    }


    fun <T : Any> from(s: String, c: Class<T>): T {
        val gson = Gson()
        return gson.fromJson(s, c)
    }

    fun <T : Any> fromToList(s: String, c: Class<T>): List<T>? {
        val gson = Gson()

        val result = ArrayList<T>()

        try {
            val ja = JSONArray(s)

            for (a in 0 until ja.length()) {
                result.add(gson.fromJson(ja.getJSONObject(a).toString(), c))
            }
        } catch (e: JSONException) {
            return null
        }

        return result
    }


}
