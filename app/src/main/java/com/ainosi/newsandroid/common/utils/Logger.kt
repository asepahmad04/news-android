package com.ainosi.newsandroid.common.utils

import com.ainosi.newsandroid.BuildConfig
import timber.log.Timber

object Logger {

    fun init(){
        if(BuildConfig.DEBUG){
            Timber.plant(Timber.DebugTree())
        }
    }

}