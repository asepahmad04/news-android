package com.ainosi.newsandroid.common.base

interface OperationCallback {
    fun onSuccess(obj:Any?)
    fun onError(obj:Any?)
}