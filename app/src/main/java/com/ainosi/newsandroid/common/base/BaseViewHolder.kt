package com.ainosi.newsandroid.common.base

import android.view.View
import androidx.recyclerview.widget.RecyclerView


abstract class BaseViewHolder<D, VH>(itemView: View) : RecyclerView.ViewHolder(itemView) {

    abstract fun onBind(holder: VH, item: D)

}