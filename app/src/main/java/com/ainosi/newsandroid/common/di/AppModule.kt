package com.ainosi.newsandroid.common.di

import com.ainosi.newsandroid.service.NewsDataSource
import com.ainosi.newsandroid.service.NewsRemoteDataSource
import com.ainosi.newsandroid.ui.main.NewsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    single<NewsDataSource> { NewsRemoteDataSource }

    viewModel { NewsViewModel(get()) }
}