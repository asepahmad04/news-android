package com.ainosi.newsandroid.common.utils

import android.content.Context
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.Glide

object CommonUtil {

    fun <T> bindView(initializer: () -> T) = lazy(LazyThreadSafetyMode.NONE, initializer)

    fun show(ctx:Context,msg:String){
        Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show()
    }

    fun loadImage(ctx: Context, uri:String?, view:ImageView){
        Glide.with(ctx).load(uri).into(view)
    }
}