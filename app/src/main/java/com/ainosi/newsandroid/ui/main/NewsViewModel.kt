package com.ainosi.newsandroid.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ainosi.newsandroid.common.base.OperationCallback
import com.ainosi.newsandroid.model.NewsModel
import com.ainosi.newsandroid.service.NewsDataSource

class NewsViewModel(private val repository: NewsDataSource):ViewModel() {

    private val _news = MutableLiveData<List<NewsModel>>().apply { value = emptyList() }
    val news: LiveData<List<NewsModel>> = _news

    private val _isViewLoading=MutableLiveData<Boolean>()
    val isViewLoading:LiveData<Boolean> = _isViewLoading

    private val _onError=MutableLiveData<Any>()
    val onError:LiveData<Any> = _onError

    private val _isEmptyList=MutableLiveData<Boolean>()
    val isEmptyList:LiveData<Boolean> = _isEmptyList

    @Suppress("UNCHECKED_CAST")
    fun getNews(params:List<Pair<String, Any?>>){
        _isViewLoading.postValue(true)
        repository.getNews(params, object:OperationCallback{

            override fun onSuccess(obj: Any?) {
                _isViewLoading.postValue(false)
                if(obj is List<*> && obj.isNotEmpty()) {
                    _news.value = obj as List<NewsModel>
                }else{
                    _isEmptyList.postValue(true)
                }
            }

            override fun onError(obj: Any?) {
                _isViewLoading.postValue(false)
                _onError.postValue(obj)
            }

        })
    }
}