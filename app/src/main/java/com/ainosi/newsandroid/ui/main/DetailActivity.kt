package com.ainosi.newsandroid.ui.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.ainosi.newsandroid.R
import com.ainosi.newsandroid.common.utils.CommonUtil
import com.ainosi.newsandroid.common.utils.Gxon
import com.ainosi.newsandroid.model.NewsModel
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity() {

    private val imgHeader by CommonUtil.bindView { findViewById<ImageView>(R.id.img_header) }
    private val tvDesc by CommonUtil.bindView { findViewById<TextView>(R.id.tvDesc) }

    companion object{
        fun toIntent(context: Context, mDetail: String) {
            val i = Intent(context, DetailActivity::class.java)
            i.putExtra("detail", mDetail)
            context.startActivity(i)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        if(intent.hasExtra("detail")){
            val mData = Gxon.from(intent.getStringExtra("detail"), NewsModel::class.java)
            CommonUtil.loadImage(this, mData.urlToImage, imgHeader)
            supportActionBar!!.title = mData.title
            tvDesc.text = mData.description
        }
    }
}
