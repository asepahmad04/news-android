package com.ainosi.newsandroid.ui.main

import android.content.Context
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.ainosi.newsandroid.R
import com.ainosi.newsandroid.common.base.BaseAdapter
import com.ainosi.newsandroid.common.base.BaseViewHolder
import com.ainosi.newsandroid.common.utils.CommonUtil
import com.ainosi.newsandroid.common.utils.Gxon
import com.ainosi.newsandroid.model.NewsModel

class MainAdapter: BaseAdapter<NewsModel, MainAdapter.NewsHolder>() {

    override fun getItemViewId(): Int = R.layout.item_list

    override fun instantiateViewHolder(view: View?): NewsHolder = NewsHolder(view!!)

    class NewsHolder(itemView: View) : BaseViewHolder<NewsModel, NewsHolder>(itemView) {
        val itemImg by lazy { itemView.findViewById<ImageView>(R.id.itemImage) }
        val itemTitle by lazy { itemView.findViewById<TextView>(R.id.itemTitle) }
        val itemDescription by lazy { itemView.findViewById<TextView>(R.id.itemDesc) }

        override fun onBind(holder: NewsHolder, item: NewsModel) {
            itemTitle?.text = item.title
            itemDescription.text = item.description
            CommonUtil.loadImage(itemView.context, item.urlToImage, itemImg)

            itemView.setOnClickListener {
                DetailActivity.toIntent( itemView.context, Gxon.to(item))
            }
        }

    }
}