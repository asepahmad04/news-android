package com.ainosi.newsandroid.ui.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.ainosi.newsandroid.R
import com.ainosi.newsandroid.common.API_KEY
import com.ainosi.newsandroid.common.utils.CommonUtil
import com.ainosi.newsandroid.model.NewsModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() , SwipeRefreshLayout.OnRefreshListener{

    private val rView by CommonUtil.bindView { findViewById<RecyclerView>(R.id.recyclerView) }
    private val swiper by CommonUtil.bindView { findViewById<SwipeRefreshLayout>(R.id.swiper) }
    private val adapters = MainAdapter()
    private val viewModel: NewsViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        rView.setHasFixedSize(true)
        rView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rView.adapter = adapters
        swiper.setOnRefreshListener(this)

        if (savedInstanceState == null) {
            viewModel.getNews(listOf("country" to "id", "apiKey" to API_KEY ))
        }
        observeLiveData()
    }

    private fun observeLiveData() {
        viewModel.isViewLoading.observe(this, Observer<Boolean> {
            it?.let {
                swiper.isRefreshing = it
            }
        })
        viewModel.news.observe(this, Observer<List<NewsModel>> {
            it?.let {
                adapters.dataSource = it
            }
        })
        viewModel.onError.observe(this, Observer<Any> {
            it?.let {
                CommonUtil.show(this, it.toString())
            }
        })
        viewModel.isEmptyList.observe(this, Observer<Any>{
            it?.let {
                CommonUtil.show(this, "Data Kosong")
            }
        })
    }

    override fun onRefresh() {
        viewModel.getNews(listOf("country" to "id", "apiKey" to API_KEY ))
    }
}
