package com.ainosi.newsandroid.service

import com.ainosi.newsandroid.common.base.OperationCallback

interface NewsDataSource {
    fun getNews(params:List<Pair<String, Any?>>,callback: OperationCallback)
}