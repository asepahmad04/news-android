package com.ainosi.newsandroid.service

import com.ainosi.newsandroid.common.BASE_URL
import com.ainosi.newsandroid.common.base.OperationCallback
import com.ainosi.newsandroid.model.ResponseNews
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.httpGet

object NewsRemoteDataSource : NewsDataSource{

    init {
        FuelManager.instance.basePath = BASE_URL
    }

    override fun getNews(params: List<Pair<String, Any?>>, callback: OperationCallback){
        "top-headlines".httpGet(params).responseObject(ResponseNews.Deserializer()) { _, _, result ->
            val (resp, err) = result
            if(resp!=null){
                if(resp.status.equals("ok"))
                    callback.onSuccess(resp.results)
                else
                    callback.onError(resp)
            }else{
                callback.onError(err)
            }
        }
    }

}
